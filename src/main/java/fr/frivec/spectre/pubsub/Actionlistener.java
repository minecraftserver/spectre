package fr.frivec.spectre.pubsub;

import fr.frivec.core.redis.pubsub.PubSubListener;
import fr.frivec.core.spectre.ServerType;
import fr.frivec.core.spectre.message.SpectreMessage;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.process.LocalProcess;
import fr.frivec.spectre.process.proxy.LocalProxy;
import fr.frivec.spectre.process.server.LocalServer;

import java.util.HashSet;

public class Actionlistener extends PubSubListener {
	
	private final Main main = Main.getInstance();
	
	@Override
	public void onMessage(CharSequence channel, String msg) {
				
		if(channel.equals("Spectre")) {
						
			final SpectreMessage message = (SpectreMessage) this.main.getGsonManager().deSeralizeJson(msg, SpectreMessage.class);

			if(message.getType().equals(SpectreMessage.SpectreMessageType.CREATE_SERVER)) {

				LocalProcess newProcess = null;

				if(message.getServerType().equals(ServerType.PROXY))

					//Creation of a new proxy
					newProcess = new LocalProxy(null, "null", message.getMaxSize(), message.getPlayerSize(), message.getPort(), message.isPermanent(), new HashSet<>());

				else {

					final LocalProcess proxyProcess = Main.getInstance().getProcessByID(message.getCurrentProxyID());

					if(proxyProcess instanceof final LocalProxy proxy) {

						newProcess = new LocalServer(null, "null", message.getServerType(), message.getPort(), message.getMaxSize(), message.getPlayerSize(), proxy, message.isPermanent());

					}

				}

				if(newProcess != null)

					newProcess.create();

			}

		}
		
	}

}
