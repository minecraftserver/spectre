package fr.frivec.spectre.process.server;

import fr.frivec.core.console.Logger.LogLevel;
import fr.frivec.core.spectre.ServerInformations;
import fr.frivec.core.spectre.ServerType;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.process.LocalProcess;
import fr.frivec.spectre.process.proxy.LocalProxy;
import fr.frivec.spectre.process.thread.ServerThread;
import org.bson.Document;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static fr.frivec.core.console.Logger.log;
import static fr.frivec.core.file.Directory.copyDirectory;

public class LocalServer extends LocalProcess {

	private LocalProxy proxy;

	public LocalServer(final UUID id, final String name, final ServerType serverType, int port, final int size, final int playerSize, final LocalProxy proxy, final boolean permanent) {

		super(id, serverType, name, port, size, playerSize, permanent);
		
		this.proxy = proxy;

	}

	@Override
	public void create() {

		/*
		 * Create a name for the server
		 *
		 */

		//Check if this name already exists

		int index = 1;
		final List<String> names = new ArrayList<>();

		for(LocalProcess currentProcess : this.main.getProcess().values())

			if(currentProcess instanceof LocalServer && currentProcess.getServerType().equals(this.serverType))

				names.add(currentProcess.getName());

		do {

			this.name = this.serverType + "-" + index + (this.isPermanent() ? "P" : "");

			index++;

		}while (names.contains(this.name));

		this.processFolder = Paths.get(this.main.getModelManager().getServerFolder().toAbsolutePath().toString(), File.separator, this.name);

		/*
		 * Create an ID for the server
		 *
		 */
		if(this.id == null)

			this.id = UUID.randomUUID();

		/*
		 * Creation of the port
		 *
		 */

		this.port = -1;

		do

			this.port = getRandomPort(); //Get a new port for the server/proxy

		while(this.main.getUsedPorts().contains(port));

		/*
		 * Database
		 *
		 */

		this.main.getServerCollection().insertOne(generateDocument());
		this.main.getProcess().put(this.id, this);

		this.main.getUsedPorts().add(this.port);

		/*
		 * Create folder for the server
		 *
		 */

		final Path sourceDirectory = Paths.get(this.main.getModelManager().getModelsRoot().toAbsolutePath().toString(), File.separator, this.serverType.getModelName());

		try {

			copyDirectory(sourceDirectory.toAbsolutePath().toString(), this.processFolder.toAbsolutePath().toString());

		} catch (IOException e) {
			e.printStackTrace();
		}

		/*
		 * Update the port in server.properties and the start file
		 *
		 */

		final Path properties = Paths.get(this.processFolder + File.separator + "server.properties"),
					startFile = Paths.get(this.processFolder + File.separator + "start.sh");

		try {

			final BufferedReader propertiesReader = Files.newBufferedReader(properties),
									startReader = Files.newBufferedReader(startFile);

			final StringBuilder propertiesBuilder = new StringBuilder(),
									startBuilder = new StringBuilder();

			String line = null;

			while((line = propertiesReader.readLine()) != null) {

				if(line.contains("server-port"))

					line = line.replace("25565", String.valueOf(this.port));

				propertiesBuilder.append(line + System.lineSeparator());

			}

			while((line = startReader.readLine()) != null) {

				if(line.contains("{name}"))

					line = line.replace("{name}", this.name);

				startBuilder.append(line + System.lineSeparator());

			}

			propertiesReader.close();
			startReader.close();

			Files.delete(properties);
			Files.createFile(properties);

			Files.delete(startFile);
			Files.createFile(startFile);

			BufferedWriter writer = Files.newBufferedWriter(properties);

			writer.write(propertiesBuilder.toString());
			writer.flush();
			writer.close();

			writer = Files.newBufferedWriter(startFile);

			writer.write(startBuilder.toString());
			writer.flush();
			writer.close();

		} catch (IOException e) {

			log(LogLevel.SEVERE, "Impossible de mettre à jour les informations du processus: " + this.name, this.getClass());
			e.printStackTrace();

		}

		/*
		 *
		 * Add the file with all process informations
		 *
		 */

		generateInformationsFile();

		/*
		 * Start the server
		 *
		 */

		start();

		/*
		 * Add the server in the proxy's list
		 *
		 */

		if(this.proxy != null) {

			this.proxy.getCurrentServers().add(this.id);
			this.proxy.save();

		}

	}

	public void save() {

		this.main.getServerCollection().findOneAndUpdate(combine(eq("_id", this.id.toString())), generateDocument());

	}

	protected Document generateDocument() {

		return new Document()
				.append("_id", this.id.toString())
				.append("name", this.name)
				.append("port", this.port)
				.append("permanent", this.permanent)
				.append("proxy", this.proxy != null ? this.proxy.getId().toString() : "null")
				.append("maxSize", this.maxSize);

	}

	protected void generateInformationsFile() {

		final Path file = Paths.get(this.processFolder.toAbsolutePath().toString() + File.separator + "spectreInformations.json");
		this.informations = new ServerInformations(this.name, this.id, (this.proxy == null ? null : this.proxy.getId()), Main.getInstance().getConfig().getMachineIP(), this.port, this.main.getConfig().getServerConfig(this.serverType).getMaxSize(), this.serverType, this.main.getConfig().getServerConfig(this.serverType).getMotd(),null);
		final String json = this.main.getGsonManager().serializeObject(this.informations);

		try {

			Files.createFile(file);

			final BufferedWriter writer = Files.newBufferedWriter(file);

			writer.write(json);
			writer.flush();
			writer.close();

		} catch (IOException e) {

			log(LogLevel.SEVERE, "Erreur. Impossible de créer le fichier spectreInformations.json du processus: " + this.name, this.getClass());
			e.printStackTrace();

		}

	}

	public ServerThread getThread() {
		return thread;
	}

	public LocalProxy getProxy() {
		return proxy;
	}

	public void setProxy(LocalProxy proxy) {
		this.proxy = proxy;
	}

}
