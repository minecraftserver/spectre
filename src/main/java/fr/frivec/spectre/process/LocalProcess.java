package fr.frivec.spectre.process;

import com.mongodb.client.FindIterable;
import fr.frivec.core.console.Logger;
import fr.frivec.core.console.Logger.LogLevel;
import fr.frivec.core.file.Directory;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.spectre.ServerInformations;
import fr.frivec.core.spectre.ServerType;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.process.proxy.LocalProxy;
import fr.frivec.spectre.process.server.LocalServer;
import fr.frivec.spectre.process.thread.ServerThread;
import org.bson.Document;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;
import static fr.frivec.core.console.Logger.log;

public abstract class LocalProcess {

	protected UUID id;
	protected String name;
	protected int port, maxSize, playerSize;
	protected boolean permanent, deleteAfterClose, running;
	
	protected ServerType serverType;

	protected Path processFolder;

	protected ServerThread thread;
	protected ServerInformations informations;

	protected Main main;

	public LocalProcess(final UUID id, final ServerType serverType, final String name, final int port, final int maxSize, final int playerSize, final boolean permanent) {

		this.id = id;
		this.serverType = serverType;
		this.name = name;
		this.port = port;
		this.maxSize = maxSize;
		this.playerSize = playerSize;
		this.permanent = permanent;
		this.running = false;

		if(this.permanent)

			this.deleteAfterClose = false;

		else

			this.deleteAfterClose = true;

		this.main = Main.getInstance();

	}

	public abstract void create();

	public abstract void save();

	public void start() {
		
		sendServerInformationsInRedis(this.informations);

		this.thread = new ServerThread(this);
		this.thread.start();

	}

	public void delete() {

		if(this instanceof LocalServer)

			this.main.getServerCollection().findOneAndDelete(eq("_id", this.id.toString()));

		else

			this.main.getProxiesCollection().findOneAndDelete(eq("_id", this.id.toString()));

		try {

			Directory.deleteFolder(this.processFolder);

		} catch (IOException e) {

			log(LogLevel.WARNING, "Erreur. Impossible de supprimer le dossier du processus " + this.name, this.getClass());
			e.printStackTrace();

		}

		Logger.log(LogLevel.INFO, "The " + this.name + "'s directory has been deleted.", this.getClass());

		return;

	}
	
	public void loadServerInformations() {
		
		final Path file = Paths.get(this.processFolder.toAbsolutePath().toString() + File.separator + "spectreInformations.json");
		
		if(Files.exists(file)) {
		
			try {
			
				final BufferedReader reader = Files.newBufferedReader(file);
				final StringBuilder builder = new StringBuilder();
				
				String line;
				
				while((line = reader.readLine()) != null)
					
					builder.append(line);
				
				this.informations = (ServerInformations) GsonManager.getInstance().deSeralizeJson(builder.toString(), ServerInformations.class);
			
			} catch (IOException e) {
				
				e.printStackTrace();
				Logger.log(LogLevel.SEVERE, "Unable to read serverInformations.json for process " + this.name + ".", getClass());
				
			}
			
		}
		
	}

	protected abstract void generateInformationsFile();

	protected abstract Document generateDocument();

	public static void loadPermanentServers() {

		final Main main = Main.getInstance();

		final FindIterable<Document> servers = main.getServerCollection().find(),
									proxys = main.getProxiesCollection().find();

		//Load proxys

		proxys.iterator().forEachRemaining(proxy -> {

			final LocalProxy localProxy = new LocalProxy(UUID.fromString(proxy.getString("_id")), proxy.getString("name"), proxy.getInteger("maxSize"), 0, proxy.getInteger("port"), proxy.getBoolean("permanent"), new HashSet<>());

			for(UUID serversInProxy : proxy.getList("servers", UUID.class))

				localProxy.getCurrentServers().add(serversInProxy);
			
			localProxy.setProcessFolder(Paths.get(main.getModelManager().getProxyFolder().toAbsolutePath().toString(), File.separator, localProxy.getName()));

			main.getProcess().put(localProxy.getId(), localProxy);

		});

		//Load servers

		servers.iterator().forEachRemaining(server -> {

			final String name = server.getString("name");
			final ServerType serverType = ServerType.getTypeByModel(name.split("[-]")[0]);

			final String proxyIDString = server.getString("proxy");
			final UUID proxyID = proxyIDString.equals("null") ? null : UUID.fromString(proxyIDString);
			final LocalProcess process = main.getProcessByID(proxyID);
			LocalProxy localProxy = null;

			if(process != null && process instanceof LocalProcess)

				localProxy = (LocalProxy) process;

			final LocalServer localServer = new LocalServer(null, name, serverType, server.getInteger("port"), server.getInteger("maxSize"), 0, localProxy, server.getBoolean("permanent"));

			localServer.setId(UUID.fromString(server.getString("_id")));
			localServer.setProcessFolder(Paths.get(main.getModelManager().getServerFolder().toAbsolutePath().toString(), File.separator, name));

			main.getProcess().put(localServer.getId(), localServer);

		});

		//Check if servers in proxys exists
		for(LocalProcess process : main.getProcess().values())

			if(process instanceof LocalProxy)

				for(UUID ids : ((LocalProxy) process).getCurrentServers())

					if(main.getProcess().containsKey(ids) && main.getProcess().get(ids) instanceof LocalServer)

						continue;
					else

						((LocalProxy) process).getCurrentServers().remove(ids);

	}
	
	protected void sendServerInformationsInRedis(final ServerInformations informations) {
		
		final RedissonClient client = this.main.getRedis().getClient();
		final RMap<String, String> rMap = client.getMap(ServerInformations.redisKey);
				
		rMap.put(this.id.toString(), GsonManager.getInstance().serializeObject(informations));
		
	}

	protected int getRandomPort() {

		final Random random = new Random();

		return random.nextInt(30000-20000) + 20000;

	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public int getPlayerSize() {
		return playerSize;
	}

	public void setPlayerSize(int playerSize) {
		this.playerSize = playerSize;
	}

	public boolean isPermanent() {
		return permanent;
	}

	public void setPermanent(boolean permanent) {
		this.permanent = permanent;
	}

	public Main getMain() {
		return main;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ServerThread getThread() {
		return thread;
	}

	public boolean deleteAfterClose() {
		return deleteAfterClose;
	}

	public void setDeleteAfterClose(boolean deleteAfterClose) {
		this.deleteAfterClose = deleteAfterClose;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public ServerInformations getInformations() {
		
		final RedissonClient client = this.main.getRedis().getClient();
		final RMap<UUID, ServerInformations> rMap = client.getMap(ServerInformations.redisKey);
		
		this.informations = rMap.get(this.id);
		
		return this.informations;
	}
	
	public void setInformations(ServerInformations informations) {
		
		this.informations = informations;
		
		final RedissonClient client = this.main.getRedis().getClient();
		final RMap<UUID, ServerInformations> rMap = client.getMap(ServerInformations.redisKey);
		
		rMap.replace(this.id, informations);
		
	}

	public Path getProcessFolder() {
		return processFolder;
	}
	
	public void setProcessFolder(Path processFolder) {
		this.processFolder = processFolder;
	}
	
	public ServerType getServerType() {
		return serverType;
	}
	
	public void setServerType(ServerType serverType) {
		this.serverType = serverType;
	}

}
