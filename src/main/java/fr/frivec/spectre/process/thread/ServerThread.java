package fr.frivec.spectre.process.thread;

import java.io.File;
import java.io.IOException;

import fr.frivec.core.console.Logger;
import fr.frivec.core.console.Logger.LogLevel;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.file.ModelManager;
import fr.frivec.spectre.process.LocalProcess;
import fr.frivec.spectre.process.server.LocalServer;

public class ServerThread extends Thread {

	private Main main;
	private LocalProcess localProcess;

	private String[] windowsStartLine, linuxStartLine;

	public ServerThread(final LocalProcess process) {

		super(process.getName());

		this.main = Main.getInstance();
		this.localProcess = process;

	}

	@Override
	public void run() {

		final ModelManager manager = this.main.getModelManager();

		this.windowsStartLine = new String[] {"cmd /c start /wait cmd.exe /C \"cd \"" + (this.localProcess instanceof LocalServer ? manager.getServerFolder().toAbsolutePath().toString() + File.separator + this.getName() : manager.getProxyFolder().toAbsolutePath().toString() + File.separator + this.getName()) + "\" && java -jar paper.jar --nogui\""};
		this.linuxStartLine = new String[] {"sh", "start.sh"};

		try {

			final ProcessBuilder builder = new ProcessBuilder();

			builder.command(this.main.useWindows() ? this.windowsStartLine : this.linuxStartLine);

			if(!this.main.useWindows())

				builder.directory(this.localProcess.getProcessFolder().toAbsolutePath().toFile());

			final Process process = builder.start();

			this.localProcess.setRunning(true);
			Logger.log(LogLevel.INFO, "The process " + this.localProcess.getName() + " is now started", this.getClass());

			process.waitFor(); //Block the thread till the server is stopped

			this.localProcess.setRunning(false);

			if(this.localProcess.deleteAfterClose()) {

				this.localProcess.delete();

				this.main.getProcess().remove(localProcess.getId());

			}

			Logger.log(LogLevel.INFO, "The process " + this.localProcess.getName() + " has been closed.", this.getClass());

		} catch (IOException e) {

			e.printStackTrace();

		} catch (InterruptedException e) {

			Logger.log(LogLevel.SEVERE, "The thread " + this.getName() + " has been interrupted before the server was closed.", this.getClass());
			e.printStackTrace();

		}

		super.run();
	}

}
