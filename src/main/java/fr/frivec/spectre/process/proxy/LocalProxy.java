package fr.frivec.spectre.process.proxy;

import fr.frivec.core.console.Logger;
import fr.frivec.core.console.Logger.LogLevel;
import fr.frivec.core.spectre.ServerInformations;
import fr.frivec.core.spectre.ServerType;
import fr.frivec.core.spectre.message.SpectreMessage;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.process.LocalProcess;
import org.bson.Document;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static fr.frivec.core.console.Logger.log;
import static fr.frivec.core.file.Directory.copyDirectory;

public class LocalProxy extends LocalProcess {

	private final Set<UUID> currentServers;

	public LocalProxy(final UUID id, final String name, final int maxSize, final int playerSize, final int port, final boolean permanent, final Set<UUID> currentServers) {

		super(id, ServerType.PROXY, name, port, maxSize, playerSize, permanent);

		this.currentServers = currentServers;

		this.main = Main.getInstance();

	}

	public void create() {

		/*
		 * Create folder for the server
		 *
		 */

		/*
		 * Create a name for the server
		 *
		 */

		//Check if this name already exists

		int index = 1;
		final List<String> names = new ArrayList<>();

		for(LocalProcess currentProcess : this.main.getProcess().values())

			if(currentProcess instanceof LocalProxy)

				names.add(currentProcess.getName());

		do {

			this.name = "PROXY-" + index + (this.isPermanent() ? "P" : "");

			index++;

		}while (names.contains(this.name));

		/*
		 * Create an ID for the server
		 *
		 */
		if(this.id == null)

			this.id = UUID.randomUUID();

		/*
		 * Creation of the port
		 *
		 */

		this.port = -1;

		do

			this.port = getRandomPort(); //Get a new port for the server/proxy

		while(this.main.getUsedPorts().contains(port));

		/*
		 * Database
		 *
		 */

		this.main.getProxiesCollection().insertOne(generateDocument());
		this.main.getProcess().put(this.id, this);

		this.main.getUsedPorts().add(this.port);

		/*
		 * Creation of the folder that contains the proxy
		 *
		 */

		this.processFolder = Paths.get(this.main.getModelManager().getProxyFolder().toAbsolutePath().toString(), File.separator, this.name);
		final Path sourceDirectory = Paths.get(this.main.getModelManager().getModelsRoot().toAbsolutePath().toString(), File.separator, ServerType.PROXY.getModelName());

		try {

			copyDirectory(sourceDirectory.toAbsolutePath().toString(), this.processFolder.toAbsolutePath().toString());

		} catch (IOException e) {
			e.printStackTrace();
		}

		/*
		 * Update the name in start.sh
		 *
		 */

		final Path startFile = Paths.get(this.processFolder + File.separator + "start.sh");

		try {

			final BufferedReader startReader = Files.newBufferedReader(startFile);

			final StringBuilder startBuilder = new StringBuilder();

			String line;

			while((line = startReader.readLine()) != null) {

				if(line.contains("{name}"))

					line = line.replace("{name}", this.name);

				startBuilder.append(line).append(System.lineSeparator());

			}

			startReader.close();

			Files.delete(startFile);
			Files.createFile(startFile);

			final BufferedWriter writer = Files.newBufferedWriter(startFile);

			writer.write(startBuilder.toString());
			writer.flush();
			writer.close();

		} catch (IOException e) {

			log(LogLevel.SEVERE, "Impossible de mettre à jour les informations du processus: " + this.name, this.getClass());
			e.printStackTrace();

		}

		/*
		 * Update the port in config.yml
		 *
		 */

		final Path config = Paths.get(this.processFolder + File.separator + "config.yml");
		final Yaml yaml = this.main.getYaml();

		try {

			final InputStream inputStream = Files.newInputStream(config);
			final Map<String, Object> map = yaml.load(inputStream);

			for(Entry<String, Object> entries : map.entrySet()) {

				if(entries.getKey().equals("listeners")) {

					if(entries.getValue() instanceof List<?>) {

						@SuppressWarnings("unchecked")
						final List<LinkedHashMap<String, Object>> list = (List<LinkedHashMap<String, Object>>) entries.getValue();

						for(LinkedHashMap<String, Object> lMap : list) {

							if(lMap.containsKey("host")) {

								lMap.remove("host");
								lMap.put("host", "0.0.0.0:" + this.port);

							}

						}

						entries.setValue(list);

					}

				}

			}

			inputStream.close();

			final PrintWriter printWriter = new PrintWriter(config.toFile());

			yaml.dump(map, printWriter);

			printWriter.close();

		} catch (IOException e) {

			Logger.log(LogLevel.SEVERE, "Unable to load the config.yml file for the proxy: " + this.name, this.getClass());
			e.printStackTrace();

		}

		/*
		 *
		 * Add the file with all process informations
		 *
		 */

		generateInformationsFile();

		/*
		 * Start the server
		 *
		 */

		start();

		/*
		 * Start a new hub if dev mode ist disabled and no hub ist registered
		 */

		if(!Main.getInstance().isDevMode()) {

			boolean isHubCreated = false;

			for(LocalProcess process : Main.getInstance().getProcess().values()) {

				if (process.getServerType().equals(ServerType.HUB)) {

					isHubCreated = true;
					break;

				}

			}

			if(!isHubCreated)

				Main.getInstance().getChannelManager().publish("Spectre", Main.getInstance().getGsonManager().serializeObject(new SpectreMessage(SpectreMessage.SpectreMessageType.CREATE_SERVER, null, null, this.id, ServerType.HUB, null, 0, 0, 0, false)));

		}

	}

	public void save() {

		this.main.getServerCollection().findOneAndReplace(combine(eq("_id", this.id.toString())), generateDocument());

	}

	protected Document generateDocument() {

		return new Document()
				.append("_id", this.id.toString())
				.append("name", this.name)
				.append("port", this.port)
				.append("permanent", this.permanent)
				.append("maxSize", this.maxSize)
				.append("servers", this.currentServers);

	}

	protected void generateInformationsFile() {
		
		final Path file = Paths.get(this.processFolder.toAbsolutePath() + File.separator + "spectreInformations.json");
		this.informations = new ServerInformations(this.name, this.id, null, Main.getInstance().getConfig().getMachineIP(),this.port, this.main.getConfig().getProxyConfig().getMaxSize(), ServerType.PROXY, this.main.getConfig().getProxyConfig().getMotd(), (HashSet<UUID>) this.currentServers);
		final String json = this.main.getGsonManager().serializeObject(this.informations);

		try {

			Files.createFile(file);

			final BufferedWriter writer = Files.newBufferedWriter(file);

			writer.write(json);
			writer.flush();
			writer.close();

		} catch (IOException e) {

			log(LogLevel.SEVERE, "Erreur. Impossible de créer le fichier spectreInformations.json du processus: " + this.name, this.getClass());
			e.printStackTrace();

		}

	}

	public Set<UUID> getCurrentServers() {
		return currentServers;
	}

}
