package fr.frivec.spectre.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import fr.frivec.core.console.Logger;
import fr.frivec.core.console.Logger.LogLevel;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.spectre.ServerType;

public class Config {
	
	private String machineIP;
	private HashMap<ServerType, ServerConfig> serversConfigs;
	private ProxyConfig proxyConfig;
	
	public Config(final String machineIP, final HashMap<ServerType, ServerConfig> serversConfigs, final ProxyConfig proxyConfig) {
		
		this.machineIP = machineIP;
		this.serversConfigs = serversConfigs;
		this.proxyConfig = proxyConfig;
	
	}
	
	public Config() {
		
		this.machineIP = "localhost";
		this.serversConfigs = new HashMap<>();
		
		for(ServerType serverType : ServerType.values()) {
			
			if(serverType.equals(ServerType.PROXY))
				
				this.proxyConfig = new ProxyConfig();
			
			else
				
				this.serversConfigs.put(serverType, new ServerConfig(serverType));
			
		}
		
	}
	
	public void saveConfig() throws IOException {
		
		final Path path = Paths.get("config.json");
		
		if(!Files.exists(path))
			
			Files.createFile(path);
		
		final BufferedWriter writer = Files.newBufferedWriter(path);
		final String json = GsonManager.getInstance().serializeObject(this);
		
		writer.write(json);
		writer.flush();
		writer.close();
			
	}
	
	public static Config loadConfig() throws IOException {
		
		final Path path = Paths.get("config.json");
		final GsonManager gsonManager = GsonManager.getInstance();
		
		if(Files.exists(path)) {
			
			final BufferedReader reader = Files.newBufferedReader(path);
			final StringBuilder builder = new StringBuilder();
			
			String line;
			
			while((line = reader.readLine()) != null)
				
				builder.append(line);
			
			reader.close();
			
			Logger.log(LogLevel.INFO, "Succesfully loaded config file", Config.class);
			
			return (Config) gsonManager.deSeralizeJson(builder.toString(), Config.class);
			
		}else {
			
			Logger.log(LogLevel.WARNING, "No config file found. Creating a new one...", Config.class);
			
			final Config config = new Config();
			
			config.saveConfig();
			
			return config;
			
		}
		
	}
	
	public ProxyConfig getProxyConfig() {
		return proxyConfig;
	}
	
	public void setProxyConfig(ProxyConfig proxyConfig) {
		this.proxyConfig = proxyConfig;
	}
	
	public HashMap<ServerType, ServerConfig> getServersConfigs() {
		return serversConfigs;
	}
	
	public String getMachineIP() {
		return machineIP;
	}
	
	public void setMachineIP(String machineIP) {
		this.machineIP = machineIP;
	}
	
	public void setServersConfigs(HashMap<ServerType, ServerConfig> serversConfigs) {
		this.serversConfigs = serversConfigs;
	}
	
	public ServerConfig getServerConfig(final ServerType serverType) {
		
		return this.serversConfigs.get(serverType);
		
	}

	public class ProxyConfig {
		
		private int maxSize; //Max players on the proxy
		private String[] motd; //2 lines max
		
		public ProxyConfig() {
			
			this.maxSize = 50;
			this.motd = new String[] {"Default motd - Change it in Spectre config", ""};
			
		}
		
		public int getMaxSize() {
			return maxSize;
		}
		
		public void setMaxSize(int maxSize) {
			this.maxSize = maxSize;
		}
		
		public String[] getMotd() {
			return motd;
		}
		
		public void setMotd(String[] motd) {
			this.motd = motd;
		}
		
	}
	
	public class ServerConfig {
		
		//TODO Add a reload config command and a way to reload infos in already running servers and proxies
		
		private int maxSize; //Max players on the proxy
		private String[] motd; //2 lines max
		private ServerType serverType;
		
		public ServerConfig(final ServerType serverType) {
			
			this.maxSize = 50;
			this.motd = new String[] {"Default motd - Change it in Spectre config", ""};
			this.serverType = serverType;
			
		}

		public int getMaxSize() {
			return maxSize;
		}

		public void setMaxSize(int maxSize) {
			this.maxSize = maxSize;
		}

		public String[] getMotd() {
			return motd;
		}

		public void setMotd(String[] motd) {
			this.motd = motd;
		}

		public ServerType getServerType() {
			return serverType;
		}

		public void setServerType(ServerType serverType) {
			this.serverType = serverType;
		}
		
	}

}
