package fr.frivec.spectre.file;

import static fr.frivec.core.console.Logger.log;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import fr.frivec.core.console.Logger.LogLevel;
import fr.frivec.spectre.Main;

public class ModelManager {

	private Path workingDirectoryParent,
				modelsRoot,
				serverFolder,
				proxyFolder;

	public ModelManager()  {

		this.workingDirectoryParent = Paths.get("").toAbsolutePath().getParent();
		this.modelsRoot = Paths.get("models");
		this.serverFolder = new File(this.workingDirectoryParent.toFile(), "server").toPath();
		this.proxyFolder = new File(this.workingDirectoryParent.toFile(), "proxy").toPath();

	}

	/**
	 * Do not call this method hen debuging on the IDE
	 */
	public void createRootDirectory() {

		try {

			if(Files.notExists(this.modelsRoot))

				Files.createDirectory(this.modelsRoot);

			if(Files.notExists(this.serverFolder)) {

				Files.createDirectory(this.serverFolder);

			}

			if(Files.notExists(this.proxyFolder)) {

				Files.createDirectory(this.proxyFolder.toAbsolutePath());

			}

		} catch (IOException e) {

			log(LogLevel.SEVERE, "Cannot create the model's root directory.", this.getClass());

			e.printStackTrace();

			Main.getInstance().setRunning(false);

		}

	}

	public void deleteDirectory(final Path path) throws IOException {

		Files.delete(path);

	}

	public Path getModelsRoot() {
		return modelsRoot;
	}

	public Path getServerFolder() {
		return serverFolder;
	}

	public Path getProxyFolder() {
		return proxyFolder;
	}

}
