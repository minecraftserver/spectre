package fr.frivec.spectre;

import com.mongodb.client.MongoCollection;
import fr.frivec.core.Credentials;
import fr.frivec.core.console.Logger.LogLevel;
import fr.frivec.core.console.commands.Command;
import fr.frivec.core.console.commands.CommandManager;
import fr.frivec.core.database.Database;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.redis.Redis;
import fr.frivec.core.redis.pubsub.PubSubManager;
import fr.frivec.core.spectre.ServerType;
import fr.frivec.core.spectre.message.SpectreMessage;
import fr.frivec.core.spectre.message.SpectreMessage.SpectreMessageType;
import fr.frivec.spectre.commands.*;
import fr.frivec.spectre.commands.dev.DevCommand;
import fr.frivec.spectre.file.Config;
import fr.frivec.spectre.file.ModelManager;
import fr.frivec.spectre.process.LocalProcess;
import fr.frivec.spectre.pubsub.Actionlistener;
import org.bson.Document;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import static fr.frivec.core.console.ConsoleColors.*;
import static fr.frivec.core.console.Logger.log;

public class Main {

	private static Main instance;

	private boolean running = true;
	private boolean useWindows = false,
					devMode = false,
					stillRunning = false;
	
	private CommandManager commandManager;
	private ModelManager modelManager;
	private Yaml yaml;
	
	private Config config;

	private Database database;
	private Redis redis;
	private Actionlistener actionlistener;
	private PubSubManager channelManager;

	private MongoCollection<Document> serverCollection,
										proxiesCollection;

	private Map<UUID, LocalProcess> process;

	private Set<Integer> usedPorts;

	public static void main(String[] args) {

		new Main(args);

	}

	public Main(String... args) {

		instance = this;
		
		/*
		 * Check if the arg -windows is present. If yes, it will change some paths and usage of Spectre.
		 * Check if Spectre has to be run as it is on a windows computer.
		 */
		
		if(args.length >= 1 && args[0].equalsIgnoreCase("-windows"))

			this.useWindows = true;

		if(args.length >= 1 && args[0].equalsIgnoreCase("-dev"))

			this.devMode = true;
		
		onEnable();
		onDisable(); //We only be used when the run() method is finished.

	}
	
	private void onEnable() {

		log(LogLevel.INFO, "Loading Spectre... Please wait.", this.getClass());

		/*
		 * Loading Managers
		 */
		new GsonManager(null, null);
		this.commandManager = new CommandManager();
		this.modelManager = new ModelManager();
		this.yaml = new Yaml();
		
		/*
		 * Load config file
		 */
		try {
		
			this.config = Config.loadConfig();
			
			System.out.println("config | adress: " + this.config.getMachineIP());
		
		} catch (IOException e) {
			
			log(LogLevel.SEVERE, "Unable to create or load the config file. Aborption..", getClass());
			
			e.printStackTrace();
			
			System.exit(0);
			
			return;
		}

		/*
		 * Creating of all model's directory if doesn't exists
		 */
		this.modelManager.createRootDirectory();

		/*
		 * Connection to the database
		 */
		this.database = new Database("localhost", "MinecraftServer");

		/*
		 * Getting the collection from database.
		 * Allows us to get data and write them on the database.
		 */
		this.serverCollection = this.database.getCollection("servers");
		this.proxiesCollection = this.database.getCollection("proxies");
		
		/*
		 * Connection to the Redis server
		 */
		this.redis = new Redis(new Credentials("localhost", "Spectre", "t3iw32ZQA3", 6379));
		this.actionlistener = new Actionlistener();
		this.channelManager = new PubSubManager(this.actionlistener);

		/*
		 * Subscribing to the Redis Pub/Sub channels
		 */
		this.channelManager.subscribe("Spectre");
		this.channelManager.subscribe("SpectreReload");

		/*
		 * HashMaps storing the currently loaded process and used ports.
		 */
		this.process = new HashMap<>();
		this.usedPorts = new HashSet<>();

		/*
		 * Registering commands.
		 */
		this.commandManager.registerCommand("test", new TestCommand());
		this.commandManager.registerCommand("stop", new StopCommand());
		this.commandManager.registerCommand("create", new CreateCommand());
		this.commandManager.registerCommand("close", new CloseCommand());
		this.commandManager.registerCommand("start", new StartCommand());
		this.commandManager.registerCommand("list", new ListCommand());
		this.commandManager.registerCommand("dev", new DevCommand());
		this.commandManager.registerCommand("reload", new ReloadCommand());

		/*
		 * If some servers are permanent, we load them so they can be started anytime.
		 */
		
		LocalProcess.loadPermanentServers();

		/*
		 * Start a public proxy if dev mode is disabled
		 */

		if(!this.devMode) {

			final SpectreMessage message = new SpectreMessage(SpectreMessageType.CREATE_SERVER, null, null, null, ServerType.PROXY, null, 0, 0, 0, false);

			//Create new proxy
			this.channelManager.publish("Spectre", this.getGsonManager().serializeObject(message));

		}

		/*
		 * Starting command listener
		 */
		run();

		log(LogLevel.INFO, "Spectre loaded.", this.getClass());
		log(LogLevel.INFO, "Welcome !", this.getClass());
		
	}
	
	private void onDisable() {
		
		/*
		 * Send a message to the running proxies to tell them to stop
		 * No need to send a message to servers, the proxies are already programmed to do it.
		 * 
		 */
		
		log(LogLevel.INFO, "Stopping all the proxies currently running.", this.getClass());
		for(LocalProcess localProcess : this.process.values()) {

			if(localProcess.isRunning() && localProcess.getServerType().equals(ServerType.PROXY)) {

				final SpectreMessage message = new SpectreMessage(SpectreMessageType.STOP_SERVER, null, localProcess.getId(), null, null, localProcess.getName(), localProcess.getPort(), 0, 0, localProcess.isPermanent());
				final String json = Main.getInstance().getGsonManager().serializeObject(message);

				this.getChannelManager().publish("Spectre", json);
				log(YELLOW + "Request sent for: " + localProcess.getName());

			}

		}

		log(LogLevel.INFO, "Close requests are now sent to all the proxies.", this.getClass());

		/*
		 * Check if all the proxies' threads are closed to stop Spectre
		 */

		do {

			this.stillRunning = false;

			final Set<Thread> runningThreads = Thread.getAllStackTraces().keySet();

			runningThreads.iterator().forEachRemaining(thread -> {

				if(thread.getName().contains(ServerType.PROXY.toString()) || thread.getName().contains(ServerType.HUB.toString())
						|| thread.getName().contains(ServerType.SKYFALL.toString()))

					this.stillRunning = true;

			});

		}while (this.stillRunning);
		
		log(LogLevel.INFO, "The process are now closed.", this.getClass());

		/*
		 * Disconnect from database
		 */

		this.database.getClient().close();
		log(LogLevel.INFO, "Disconnected from the database", this.getClass());


		/*
		 * Unsubscribe from the pub/sub channel
		 */
		this.channelManager.unsubscribe("Spectre"); 
		
		/*
		 * Disconnect from Redis
		 */
		
		this.redis.close(); //Close redis connection
		log(LogLevel.INFO, "Disconnected from Redis server.", this.getClass());

		log(LogLevel.INFO, "Spectre is now desactivated. Bis später.", this.getClass());
		
	}
	
	private void run() {
		
		final Scanner scanner = new Scanner(System.in);

		while (this.running) {

			if(scanner.hasNextLine())

				listenCommands(scanner.nextLine());

		}
		
		scanner.close();
		log(LogLevel.INFO, "Stopped the command listener", this.getClass());
		
	}

	private void listenCommands(final String line) {

		if(line != null && !line.equalsIgnoreCase("")) {

			final String[] args = line.split("[ ]");
			final String commandName = args[0];

			//Start searching for the command to call
			for(Entry<String, Command> entries : this.commandManager.getCommands().entrySet()) {

				if(entries.getKey().equalsIgnoreCase(commandName)) {

					entries.getValue().onCommand(args);

					return;

				}

			}

			log(RED + "Erreur. Aucune commande portant ce nom n'a été trouvé." + RESET);
			return;

		}

	}

	public LocalProcess getProcessByID(final UUID id) {

		for(Entry<UUID, LocalProcess> entries : this.process.entrySet())

			if(entries.getKey().equals(id))

				return entries.getValue();

		return null;

	}

	public LocalProcess getProcessByName(final String name) {

		for(LocalProcess localProcess : this.process.values())

			if(localProcess.getName().equalsIgnoreCase(name))

				return localProcess;

		return null;

	}

	public static Main getInstance() {
		return instance;
	}

	public Redis getRedis() {
		return redis;
	}
	
	public Config getConfig() {
		return config;
	}
	
	public void setConfig(Config config) {
		this.config = config;
	}
	
	public CommandManager getCommandManager() {
		return commandManager;
	}

	public GsonManager getGsonManager() {
		return GsonManager.getInstance();
	}

	public PubSubManager getChannelManager() {
		return channelManager;
	}

	public ModelManager getModelManager() {
		return modelManager;
	}

	public Yaml getYaml() {
		return yaml;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public boolean isRunning() {
		return running;
	}

	public Database getDatabase() {
		return database;
	}

	public MongoCollection<Document> getServerCollection() {
		return serverCollection;
	}

	public MongoCollection<Document> getProxiesCollection() {
		return proxiesCollection;
	}

	public Map<UUID, LocalProcess> getProcess() {
		return process;
	}

	public Set<Integer> getUsedPorts() {
		return usedPorts;
	}

	public boolean useWindows() {
		return useWindows;
	}

	public boolean isDevMode() {
		return devMode;
	}
}
