package fr.frivec.spectre.commands;

import static fr.frivec.core.console.ConsoleColors.GREEN;
import static fr.frivec.core.console.ConsoleColors.RED;
import static fr.frivec.core.console.Logger.log;

import java.io.IOException;

import fr.frivec.core.console.commands.Command;
import fr.frivec.core.json.GsonManager;
import fr.frivec.core.redis.messages.SpectreReloadMessage;
import fr.frivec.core.spectre.ServerType;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.file.Config;

public class ReloadCommand extends Command {
	
	public ReloadCommand() {
		
		super("reload", "Reload some parts of Spectre like the config", "reload <config>");
		
	}

	@Override
	public void onCommand(String[] args) {
		
		if(args.length > 0) {
			
			if(args[1].equalsIgnoreCase("config")) {
				
				Config config;
				
				/*
				 * Load the new config
				 */
				
				try {
				
					config = Config.loadConfig();

				} catch (IOException e) {
				
					log(RED + "Une erreur est survenue. Le fichier de configuration ne peut pas être lu par Spectre.");
					e.printStackTrace();
					
					return;
					
				}
				
				//Set the new config in Main
				Main.getInstance().setConfig(config);
				
				final SpectreReloadMessage message = new SpectreReloadMessage();
				
				for(ServerType type : ServerType.values()) {
					
					if(type.equals(ServerType.PROXY)) {
					
						message.getMotd().put(type, config.getProxyConfig().getMotd());
						message.getMaxSize().put(type, config.getProxyConfig().getMaxSize());
					
					}else {
						
						message.getMotd().put(type, config.getServerConfig(type).getMotd());
						message.getMaxSize().put(type, config.getServerConfig(type).getMaxSize());
						
					}
						
				}
				
				Main.getInstance().getChannelManager().publish("SpectreReload", GsonManager.getInstance().serializeObject(message));
				
				log(GREEN + "La nouvelle configuration a bien été envoyée à tous les serveurs en ligne.");
				
				return;
				
			}else {
				
				log(RED + "Erreur. Merci d'utiliser des arguments valides.");
				log(RED + "Utilisation: " + this.usage);
				return;
				
			}
			
		}else {
			
			log(RED + "Erreur. Merci d'utiliser des arguments valides.");
			log(RED + "Utilisation: " + this.usage);
			return;
			
		}

	}

}
