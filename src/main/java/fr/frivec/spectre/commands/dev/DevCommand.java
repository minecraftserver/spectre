package fr.frivec.spectre.commands.dev;

import static fr.frivec.core.console.ConsoleColors.GREEN;
import static fr.frivec.core.console.ConsoleColors.RED;
import static fr.frivec.core.console.Logger.log;

import fr.frivec.core.console.commands.Command;
import fr.frivec.spectre.Main;

public class DevCommand extends Command {

	public DevCommand() {
		super("Dev", "Command to test some features. Do not use it for common usage.", "dev <argument>");
	}
	
	@Override
	public void onCommand(String[] args) {
				
		if(args.length > 0) {
			
			if(args[1].equalsIgnoreCase("start")) {
				
				log(GREEN + "Trying to start PROXY-1P and HUB-1P");
				
				final Command command = Main.getInstance().getCommandManager().getCommands().get("start");
				
				command.onCommand(new String[] {"", "proxy-1p"});
				command.onCommand(new String[] {"", "hub-1p"});
				
				return;
				
			}
			
		}else {
			
			log(RED + "Erreur. Merci d'utiliser des arguments valides.");
			log(RED + "Utilisation: " + this.usage);
			return;
			
		}			

	}

}
