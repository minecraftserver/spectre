package fr.frivec.spectre.commands;

import static fr.frivec.core.console.ConsoleColors.CYAN;
import static fr.frivec.core.console.ConsoleColors.RED;
import static fr.frivec.core.console.ConsoleColors.YELLOW;
import static fr.frivec.core.console.Logger.log;

import fr.frivec.core.console.commands.Command;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.process.LocalProcess;
import fr.frivec.spectre.process.proxy.LocalProxy;
import fr.frivec.spectre.process.server.LocalServer;

public class ListCommand extends Command {

	public ListCommand() {

		super("List", "List all servers and proxys loaded", "list (server/proxy)");

	}

	@Override
	public void onCommand(String[] args) {

		final Main main = Main.getInstance();

		if(args.length >= 1) {

			if(args.length == 1) {

				if(main.getProcess().size() == 0)

					log(RED + "but nobody came");

				//List all process loaded
				for(LocalProcess currentProcess : main.getProcess().values())

					log(CYAN + (currentProcess instanceof LocalServer ? "SERVER" : "PROXY") + " " + currentProcess.getName() + " | ID: " + currentProcess.getId() + " | Status: "
						+ (currentProcess.isRunning() ? "Running" : "Closed") + " | Permanent: " + currentProcess.isPermanent() + " | Port: " + currentProcess.getPort());

				return;

			}else {

				int size = 0;

				//List all proxys or all servers
				if(args[1].equalsIgnoreCase("server")) {

					for(LocalProcess currentProcess : main.getProcess().values()) {

						if(currentProcess instanceof LocalServer) {

							log(CYAN + "SERVER " + currentProcess.getName() + " | ID: " + currentProcess.getId() + " | Status: "
									+ (currentProcess.isRunning() ? "Running" : "Closed") + " | Permanent: " + currentProcess.isPermanent() + " | Port: " + currentProcess.getPort());

							size++;

						}

					}

					if(size == 0)

						log(RED + "Aucun serveur n'est encore arrivé. Le restaurant est encore fermé.");

					return;

				}else if(args[1].equalsIgnoreCase("proxy")) {

					for(LocalProcess currentProcess : main.getProcess().values()) {

						if(currentProcess instanceof LocalProxy) {

							log(CYAN + "PROXY " + currentProcess.getName() + " | ID: " + currentProcess.getId() + " | Status: "
									+ (currentProcess.isRunning() ? "Running" : "Closed") + " | Permanent: " + currentProcess.isPermanent() + " | Port: " + currentProcess.getPort());

							size++;

						}

					}

					if(size == 0)

						log(RED + "It seems that nobody's awake");

					return;

				}else

					log(YELLOW + "Erreur. Merci d'entrer un type de serveur valide: server ou proxy.");

				return;

			}

		}else {

			log(RED + "Erreur. Merci d'utiliser des arguments valides.");
			log(RED + "Utilisation: " + this.usage);
			return;

		}

	}

}
