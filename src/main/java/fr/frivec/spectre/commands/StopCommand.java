package fr.frivec.spectre.commands;

import static fr.frivec.core.console.Logger.log;

import fr.frivec.core.console.Logger.LogLevel;
import fr.frivec.core.console.commands.Command;
import fr.frivec.spectre.Main;

public class StopCommand extends Command {
	
	public StopCommand() {
		
		super("Stop", "Stop the Spectre program", "stop");
	
	}
	
	@Override
	public void onCommand(String[] args) {
		
		if(!Main.getInstance().isRunning()) {
			
			log(LogLevel.WARNING, "Spectre is already stopping. Please wait.", this.getClass());
			
			return;
			
		}
		
		log(LogLevel.INFO, "The program will stop.", null);
		Main.getInstance().setRunning(false);
		
		return;
	}

}
