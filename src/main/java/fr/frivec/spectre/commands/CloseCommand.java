package fr.frivec.spectre.commands;

import fr.frivec.core.console.commands.Command;
import fr.frivec.core.spectre.message.SpectreMessage;
import fr.frivec.core.spectre.message.SpectreMessage.SpectreMessageType;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.process.LocalProcess;

import java.util.UUID;

import static fr.frivec.core.console.ConsoleColors.*;
import static fr.frivec.core.console.Logger.log;

public class CloseCommand extends Command {

	public CloseCommand() {

		super("Close", "A command to close a server or a process that is running", "close <serverName/serverID> (delete (Only for permanent server))");

	}

	@Override
	public void onCommand(String[] args) {

		if(args.length > 1) {

			final String strID = args[1];

			LocalProcess localprocess;
			boolean delete = false;

			if(args.length >= 3)

				delete = Boolean.parseBoolean(args[2]);

			try {

				//Check with the first method (UUID)
				final UUID processID = UUID.fromString(strID);

				localprocess = Main.getInstance().getProcessByID(processID);

			} catch (IllegalArgumentException e) {localprocess = null;}

			//Check with the second method (Name)
			if(localprocess == null)

				localprocess = Main.getInstance().getProcessByName(strID);

			if(localprocess == null) {

				log(RED + "Erreur. Aucun processus n'a été trouvé avec cet ID ou ce nom.");
				return;

			}

			/*
			 * Check if the process is running
			 *
			 */

			if(!localprocess.isRunning()) {

				log(YELLOW + "Error. The server you want to close is already closed.");
				return;

			}

			/*
			 * Delete the folder if permanent
			 *
			 */

			if(localprocess.isPermanent() && delete)

				localprocess.setDeleteAfterClose(true);

			/*
			 * Send message to the server to tell him to shutdown
			 *
			 */

			final SpectreMessage message = new SpectreMessage(SpectreMessageType.STOP_SERVER, null, localprocess.getId(), null, null, localprocess.getName(), localprocess.getPort(), 0, 0, localprocess.isPermanent());
			final String json = Main.getInstance().getGsonManager().serializeObject(message);

			Main.getInstance().getChannelManager().publish("Spectre", json);

			log(GREEN + "Close request for " + localprocess.getName() + " has been sent succesfully.");

		}else {

			log(RED + "Erreur. Merci d'utiliser des arguments valides.");
			log(RED + "Utilisation: " + this.usage);

		}

	}

}
