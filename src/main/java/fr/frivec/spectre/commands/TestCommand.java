package fr.frivec.spectre.commands;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import fr.frivec.core.console.commands.Command;
import fr.frivec.core.database.Database;
import fr.frivec.core.spectre.ServerType;
import fr.frivec.core.spectre.message.SpectreMessage;
import fr.frivec.core.spectre.message.SpectreMessage.SpectreMessageType;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.process.LocalProcess;
import fr.frivec.spectre.process.proxy.LocalProxy;
import fr.frivec.spectre.process.server.LocalServer;
import org.bson.Document;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

public class TestCommand extends Command {

	public TestCommand() {
		
		super("test", "A command to test the features of the program", "test <param1> <param2>");
		
	}
	
	@Override
	public void onCommand(final String[] args) {
		
		if(args.length > 1) {
		
			//First arg is args[1] and not args[0] !
			if(args[1].equalsIgnoreCase("db")) {
				
				final Database database = new Database("localhost", "spectre");
				final MongoCollection<Document> testCollection = database.getCollection("servers");
				
				testCollection.insertOne(new Document().append("_id", 1).append("name", "Frivec").append("money", 1500));
				testCollection.insertOne(new Document().append("_id", 2).append("name", "CraftZx2").append("money", 999));
				testCollection.insertOne(new Document().append("_id", 3).append("name", "Crafti").append("money", 80970));
				
				testCollection.updateOne(eq("_id", 1), combine(set("name", "Galix78"), set("money", 1000)));
				
				final FindIterable<Document> documents = testCollection.find(eq("name", "Frivec"));
				
				if(documents.first() == null)
					
					System.out.println("Not found");
				
				for(Document document : documents)
					
					System.out.println("Document: " + document.getString("name") + " | Money: " + document.getInteger("money", -1));
				
				return;
				
			}else if(args[1].equalsIgnoreCase("server")) {
				
				final LocalProxy localProxy = new LocalProxy(UUID.fromString("e9d8293b-096d-402d-9202-a88bcc1af50f"), "TestProxy", 20, 0, 25565, true, new HashSet<>());
				final LocalServer localServer = new LocalServer(null, "TestServer", ServerType.HUB, 25577, 20, 0, localProxy, true);
				
				localProxy.create();
				localServer.create();
				
				localProxy.getCurrentServers().add(localServer.getId());
				
				localProxy.save();
				
				return;
				
			}else if(args[1].equalsIgnoreCase("pubsub")) {
				
				Main.getInstance().getChannelManager().publish("Spectre", Main.getInstance().getGsonManager().serializeObject(new SpectreMessage(SpectreMessageType.START_SERVER, null, null, UUID.fromString("e9d8293b-096d-402d-9202-a88bcc1af50f"), ServerType.HUB, "AnotherTestServer", 20, 0, 0, false)));
				
				return;
				
			}else if(args[1].equalsIgnoreCase("execute")) {
				
				new TestThread().start(); //launch the server
				
				return;
				
			}else if(args[1].equalsIgnoreCase("getthread")) {
				
				final Set<Thread> runningThreads = Thread.getAllStackTraces().keySet();
				
				runningThreads.iterator().forEachRemaining(thread -> System.out.println(thread.getName()));
				
				return;
				
			}else if(args[1].equalsIgnoreCase("cmd")) {
				
				final Runtime runtime = Runtime.getRuntime();
								
				try {
					
					@SuppressWarnings("unused")
					final Process process = runtime.exec("cmd /c start cmd.exe /C \"cd \"D:/Developpement/Minecraft/NetworkProject/server/HUB-1\" && java -jar paper.jar --nogui\"");
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				return;
				
			}else if(args[1].equalsIgnoreCase("yml")) {
				
				final Path file = Paths.get("bukkit.yml");
				
				try {
				
					final InputStream inputStream = Files.newInputStream(file);
					final Yaml yaml = Main.getInstance().getYaml();
					
					final Map<String, Object> map = yaml.load(inputStream);
					
					for(Entry<String, Object> entries : map.entrySet())
						
						if(entries.getKey().equals("id"))
							
							entries.setValue(10);
					
					final PrintWriter printWriter = new PrintWriter(file.toFile());
					
					yaml.dump(map, printWriter);
					
					return;
					
				} catch (IOException e) {
					
					e.printStackTrace();
					
				}
				
			}else if(args[1].equalsIgnoreCase("list")) {
				
				for(LocalProcess localProcess : Main.getInstance().getProcess().values())
					
					System.out.println(localProcess.getName() + " | " + localProcess.getPort());
				
			}
		
		}else {
			
			System.err.println("Erreur. Merci d'ajouter un argument");
			return;
			
		}
		
		System.err.println("Erreur. Merci d'entrer un argument valide");
		System.err.println("Utilisation: " + this.usage);
		return;
		
	}
	
	private class TestThread extends Thread {
		
		public TestThread() {
			
			super("TestThread");
			
		}
		
		@Override
		public void run() {
			
			final ProcessBuilder builder = new ProcessBuilder("java", "-jar", "paper.jar"); //Create a process that contains the name and the args of the start command
			builder.directory(new File("D:\\Developpement\\Minecraft\\Serveurs\\Serveurs_Spigot\\Serveur_1.16.5")); //Set the file where the command will be execute
			
			try {
				
				final Process process = builder.start(); //Start the process
				
				process.waitFor(); //Block the thread till the server is stopped
				
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
			
			super.run();
		}
		
	}
	
//    private String readProcessOutput(Process p) throws Exception{
//    	
//        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
//        String response = "";
//        String line;
//        while ((line = reader.readLine()) != null) {
//            response += line+"\r\n";
//        }
//        reader.close();
//        return response;
//        
//    }

}
