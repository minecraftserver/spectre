package fr.frivec.spectre.commands;

import static fr.frivec.core.console.ConsoleColors.BLUE;
import static fr.frivec.core.console.ConsoleColors.GREEN;
import static fr.frivec.core.console.ConsoleColors.RED;
import static fr.frivec.core.console.ConsoleColors.YELLOW;
import static fr.frivec.core.console.Logger.log;

import java.util.UUID;

import fr.frivec.core.console.commands.Command;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.process.LocalProcess;

public class StartCommand extends Command {
	
	private static StartCommand instance;
	
	public StartCommand() {
		
		super("Start", "Start a permanent server that already exists", "start <id/name>");
		
		instance = this;
		
	}

	@Override
	public void onCommand(String[] args) {
		
		if(args.length > 1) {
			
			final String strID = args[1];
			
			LocalProcess localProcess = null;
			
			try {
				
				//Check with the first method (UUID)
				final UUID processID = UUID.fromString(strID);
				
				localProcess = Main.getInstance().getProcessByID(processID);
				
			} catch (IllegalArgumentException e) {localProcess = null;}
			
			//Check with the second method (Name)
			if(localProcess == null)
				
				localProcess = Main.getInstance().getProcessByName(strID);
			
			if(localProcess == null) {
				
				log(RED + "Erreur. Aucun processus n'a été trouvé avec cet ID ou ce nom.");
				return;
				
			}
			
			/*
			 * Check if the process is running
			 * 
			 */
			
			if(localProcess.isRunning()) {
				
				log(YELLOW + "Error. The server you want to start is already running.");
				return;
				
			}
			
			localProcess.loadServerInformations();
			localProcess.start();
			
			log(GREEN + "The process " + localProcess.getName() + " is opening. Please wait.");
			log(BLUE + "Informations:");
			log(BLUE + "name: " + localProcess.getName());
			log(BLUE + "port: " + localProcess.getPort());
			log(BLUE + "permanent: " + localProcess.isPermanent());
			
			return;
			
		}else {
			
			log(RED + "Erreur. Merci d'utiliser des arguments valides.");
			log(RED + "Utilisation: " + this.usage);
			return;
			
		}

	}
	
	public static StartCommand getInstance() {
		return instance;
	}

}
