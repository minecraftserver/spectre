package fr.frivec.spectre.commands;

import fr.frivec.core.console.commands.Command;
import fr.frivec.core.spectre.ServerType;
import fr.frivec.spectre.Main;
import fr.frivec.spectre.process.LocalProcess;
import fr.frivec.spectre.process.proxy.LocalProxy;
import fr.frivec.spectre.process.server.LocalServer;

import java.util.HashSet;
import java.util.UUID;

import static fr.frivec.core.console.ConsoleColors.*;
import static fr.frivec.core.console.Logger.log;

public class CreateCommand extends Command {
	
	public CreateCommand() {
		
		super("Create", "A command to create servers and proxies", "create <server/proxy> <modelName> <permanent> (proxyID)");
	
	}
	
	@Override
	public void onCommand(String[] args) {
		
		if(args.length >= 4) {
			
			final String type = args[1],
						modelsName = args[2];
			
			final boolean permanent = Boolean.parseBoolean(args[3]);
			final ServerType serverType = ServerType.getTypeByModel(modelsName);
			
			LocalProcess localProcess;
						
			if(type.equalsIgnoreCase("server")) {
				
				LocalProcess localProxy;
				
				if(args.length < 5 || args[4] == null || args[4].equals("")) {
					
					log(RED + "Erreur. Merci d'indiquer l'ID ou le nom d'un proxy existant pour rattacher ce serveur." + RESET);
					log(YELLOW + "Si vous ne voulez pas rattacher ce serveur à un proxy, indiquez null." + RESET);
					
					return;
					
				}
				
				if(serverType == null) {
					
					log(RED + "Erreur. Le modèle spécifié est introuvable. Veuillez réessayer.");
					return;
					
				}
				
				final String proxyName = args[4];
				
				if(proxyName.equalsIgnoreCase("null"))
					
					localProxy = null;
				
				else {
					
					try {
						
						//Check with the first method (UUID)
						final UUID proxyID = UUID.fromString(proxyName);
						
						localProxy = Main.getInstance().getProcessByID(proxyID);
						
					} catch (IllegalArgumentException e) {localProxy = null;}
					
					//Check with the second method (Name)
					if(localProxy == null)
						
						localProxy = Main.getInstance().getProcessByName(proxyName);
					
					if(localProxy == null) {
						
						log(RED + "Erreur. Aucun proxy avec ce nom ou cet ID n'a été trouvé." + RESET);
						return;
						
					}else if(localProxy instanceof LocalServer) {
					
						log(RED + "Erreur. Cet ID ou ce nom renvoie vers un serveur et non un proxy." + RESET);
						return;
						
					}
					
				}
			
				localProcess = new LocalServer(null, "null", serverType, -1, 0, 0, (LocalProxy) localProxy, permanent);
			
			}else if(type.equalsIgnoreCase("proxy"))
				
				localProcess = new LocalProxy(null, "null", 0, 0, 0, permanent, new HashSet<>());
			
			else {
					
				log(RED + "Erreur. Impossible de trouver le type de processus que vous souhaitez lancer. Veuillez recommencer." + RESET);
				return;
					
			}
			
			log(GREEN + "Your process is creating. Please wait for it to open.");
			localProcess.create();
			log(GREEN + "Your " + (localProcess instanceof LocalServer ? "server" : "proxy") + " has been created.");
			log(BLUE + "Informations:");
			log(BLUE + "name: " + localProcess.getName());
			log(BLUE + "port: " + localProcess.getPort());
			log(BLUE + "permanent: " + localProcess.isPermanent());
			
		}else {
			
			log(RED + "Erreur. Merci d'utiliser des arguments valides." + RESET);
			log(RED + "Utilisation: " + this.usage + RESET);
			
		}
		
		return;
		
	}

}
