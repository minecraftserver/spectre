package fr.frivec;

import java.io.IOException;
import java.nio.file.Paths;

public class Main {

	public static void main(String[] args) {

		new Main();

	}

	public Main() {

		final ProcessBuilder builder = new ProcessBuilder();

		builder.directory(Paths.get("/home/antoine/Bureau/Developpement/Minecraft/MinecraftNetwork/server/HUB-TEST/").toFile());
		builder.command("bash", "-c", "sh start.sh");

		try {

			builder.start();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
